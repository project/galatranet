﻿This module is a group of modules that helps you handle some of the common tasks of an organization.
The common tasks such as Employees Record Management, Employees Leave Management, Contract Document Creation and many more.

This module would be of great help to SME enterprises to help automate their HR tasks, contract creation and many more.

Initially release would contain Leave Management System followed by Employee Record Management.
Later on we intend to add Document management.

Feedback and feature request are welcome.

The project has been developed by Intern Students from Ahmedabad, Gujarat, India, Smruti Shah and Pooja Mehta
under guidance of Barinder Singh and Dheeraj Dagliya.

The development of the project is being sponsored by Galaminds.


Features of Leave Management System:

    1.) The role who has all the rights i.e. Administrator can define specific day off.
    2.) Administrator can define the leave types according to its organization.
    3.) Administrator can assign leaves according to the role.
    4.) Administrator can also grant extra leaves to the particular user on the basis of his/her performance.
    5.) Administrator can see the reports for pending leave application and can take actions for that leave application.
        Whatever decision is taken is also informed to the user by mail.
    6.) The final decision is taken by Administrator, if he/she has not taken any decision then the person
        who has selected in reportto field by user, will take the decision. 
    7.) Administrator can see the reports of past leave application, leaves which are cancelled by users and
        leave summary of all the users.
    8.) The user can apply for the leave application from create content  >> leave application
    9.) This leave application would  be mailed to the Administrator as well as the person that was selected by the applicant in the reportto field.
    10.) User can view the list of holidays.
    11.) User can view the reports of his/her leave status and summary of his/her leaves.
    12.) User can also edit or cancel his/her leave application.
