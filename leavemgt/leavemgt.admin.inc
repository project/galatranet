<?php
function leavemgt_admin($callback_arg = '') {
  $op = isset($_POST['op']) ? $_POST['op'] : $callback_arg;
  $output = drupal_get_form('leavemgt_filter_form');
  $output .= drupal_get_form('leavemgt_admin_account');
  return $output;
}

function leavemgt_filter_form() {
  $session = &$_SESSION['leavemgt_overview_filter'];
  $session = is_array($session) ? $session : array();
  $filters = leavemgt_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only users where'),
    '#theme' => 'leavemgt_filters',
  );
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    // Merge an array of arrays into one if necessary.
    $options = $type == 'permission' ? call_user_func_array('array_merge', $filters[$type]['options']) : $filters[$type]['options'];
    $params = array('%property' => $filters[$type]['title'] , '%value' => $options[$value]);
    if ($i++ > 0) {
      $form['filters']['current'][] = array('#value' => t('<em>and</em> where <strong>%property</strong> is <strong>%value</strong>', $params));
    }
    else {
      $form['filters']['current'][] = array('#value' => t('<strong>%property</strong> is <strong>%value</strong>', $params));
    }
  }
  foreach ($filters as $key => $filter) {
    $names[$key] = $filter['title'];
    $form['filters']['status'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
    );
  }
$form['filters']['filter'] = array(
  '#type' => 'radios',
  '#options' => $names,
);
$form['filters']['buttons']['submit'] = array(
  '#type' => 'submit',
  '#value' => (count($session) ? t('Refine') : t('Filter')),
);
if (count($session)) {
$form['filters']['buttons']['undo'] = array(
  '#type' => 'submit',
  '#value' => t('Undo'),
    );
$form['filters']['buttons']['reset'] = array(
  '#type' => 'submit',
  '#value' => t('Reset'),
    );
  }
drupal_add_js('misc/form.js', 'core');
return $form;
}

function leavemgt_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = leavemgt_filters();
  switch ($op) {
    case t('Filter'): case t('Refine'):
      if (isset($form_state['values']['filter'])) {
        $filter = $form_state['values']['filter'];
        // Merge an array of arrays into one if necessary.
        $options = $filter == 'permission' ? call_user_func_array('array_merge', $filters[$filter]['options']) : $filters[$filter]['options'];
        if (isset($options[$form_state['values'][$filter]])) {
          $_SESSION['leavemgt_overview_filter'][] = array($filter, $form_state['values'][$filter]);
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['leavemgt_overview_filter']);
      break;
    case t('Reset'):
      $_SESSION['leavemgt_overview_filter'] = array();
      break;
    case t('Update'):
      return;
  }

  $form_state['redirect'] = 'leavemgt/leavesummary';
  return;
}

/**
 * Theme leavemgt administration filter selector.
*/
function theme_leavemgt_filter_form($form) {
  $output = '<div id="leavemgt-admin-filter">';
  $output .= drupal_render($form['filters']);
  $output .= '</div>';
  $output .= drupal_render($form);
  return $output;
}
function theme_leavemgt_filters($form) {

  $output = '<ul class="clear-block">';
  if (!empty($form['current'])) {
    foreach (element_children($form['current']) as $key) {
      $output .= '<li>'. drupal_render($form['current'][$key]) .'</li>';
    }
  }

  $output .= '<li><dl class="multiselect">'. (!empty($form['current']) ? '<dt><em>'. t('and') .'</em> '. t('where') .'</dt>' : '') .'<dd class="a">';
  foreach (element_children($form['filter']) as $key) {
    $output .= drupal_render($form['filter'][$key]);
  }
  $output .= '</dd>';

  $output .= '<dt>'. t('is') .'</dt><dd class="b">';

  foreach (element_children($form['status']) as $key) {
    $output .= drupal_render($form['status'][$key]);
  }
  $output .= '</dd>';

  $output .= '</dl>';
  $output .= '<div class="container-inline" id="user-admin-buttons">'. drupal_render($form['buttons']) .'</div>';
  $output .= '</li></ul>';

  return $output;
}

function leavemgt_admin_account() {
  $filter = leavemgt_build_filter_query();

  $header = array(
    array(),
    array('data' => t('Username'), 'field' => 'u.name'),
    t('Roles'),
    array('data' => t('Leaves Allowed')),
    array('data' => t('Extra Assigned Leave')),
    array('data' => t('Total Leaves')),
    array('data' => t('Leaves Taken')),
    array('data' => t('Paid Leaves')),
    array('data' => t('Non paid Leaves')),
    array('data' => t('Balance')),
   
  );

  $sql = 'SELECT DISTINCT u.uid, u.name, u.status, u.created, u.access FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid '. $filter['join'] .' WHERE u.uid > 1 ' . $filter['where'];
  $sql .= tablesort_sql($header);
  $query_count = 'SELECT COUNT(DISTINCT u.uid) FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid '. $filter['join'] .' WHERE u.uid > 1 '. $filter['where'];
  $result = pager_query($sql, 50, 0, $query_count, $filter['args']);

  $destination = drupal_get_destination();
  $roles = user_roles(TRUE);
  $accounts = array();
  while ($account = db_fetch_object($result)) {
    $accounts[$account->uid] = '';

    $form['name'][$account->uid] = array('#value' => theme('username', $account));
    $users_roles = array();
    $roles_result = db_query('SELECT rid FROM {users_roles} WHERE uid = %d', $account->uid);
      while ($user_role = db_fetch_object($roles_result)) {
      $users_roles[] = $roles[$user_role->rid];
    }
    asort($users_roles);
   $form['roles'][$account->uid][0] = array('#value' => theme('item_list', $users_roles));
   $leavetype=array();
   $assleave=array();
  
   $query=db_query('SELECT max(al.maxallowleave) as max,lt.leavetype,lt.leavetypeid,al.designation_rid from {assignleave}  al,{users_roles} ur,{leavetype} lt where ur.uid=%d and al.designation_rid=ur.rid and al.leavetype_tid=lt.leavetypeid group by al.leavetype_tid',$account->uid);
   
   while($data = db_fetch_object($query)){
    $leavetype[$data->leavetype] = $data->leavetype;
    $assleave[$data->leavetypeid] = $data->max;
    $form['leavetype'][$account->uid][0] = array('#value' => theme('item_list', $leavetype));
    $form['assleave'][$account->uid][0] = array('#value' =>  theme('item_list', $assleave));
    
    $res=db_query('SELECT sum(xl.leavegranted) as total,lt.leavetypeid,xl.uid from {xtraleave}  xl,{leavetype}  lt WHERE xl.leavetypeid=lt.leavetypeid and xl.uid=%d group by xl.leavetypeid',$account->uid);
    $grantextra = array();
    while($data1 = db_fetch_object($res)) {
      
      $grantextra[$data1->leavetypeid]= $data1->total;
      if(!$grantextra[$data1->leavetypeid]){
        $grantextra[$data1->leavetypeid]=0;
      }
     }
    $form['grantextra'][$account->uid][0] = array('#value' =>  theme('item_list', $grantextra));
 
   }
  $tot=array();  
  $rs=db_query("SELECT al.leavetype_tid,ur.uid from {leavetype} lt,{users_roles} ur,{assignleave} al WHERE lt.leavetypeid=al.leavetype_tid AND al.designation_rid=ur.rid AND ur.uid=%d",$account->uid);
  while($data=db_fetch_object($rs)){
    $tot[$data->leavetype_tid]=$grantextra[$data->leavetype_tid] +$assleave[$data->leavetype_tid] ;
  }
  $form['totalleave'][$account->uid][0] = array('#value' =>  theme('item_list', $tot));
  
  $leave1 = db_query('SELECT sum(la.totaldays) as total,la.leavetype_tid,n.uid FROM {leaveapp} la,{node} n WHERE la.nid=n.nid AND n.uid= %d AND la.status=%d group by la.leavetype_tid',$account->uid,1);
  $leaves_taken = array();
  $paidleaves=array();
  $nonpaidleaves=array();
  while($result1 = db_fetch_object($leave1)){
    $leaves_taken[$result1->leavetype_tid]=$result1->total;
  if($leaves_taken[$result1->leavetype_tid]>$tot[$result1->leavetype_tid])
  {
    $paidleaves[$result1->leavetype_tid]=$tot[$result1->leavetype_tid];
    $nonpaidleaves[$result1->leavetype_tid]=$leaves_taken[$result1->leavetype_tid]-$tot[$result1->leavetype_tid];
  }
  else
  {
    $paidleaves[$result1->leavetype_tid]=$leaves_taken[$result1->leavetype_tid];
    $nonpaidleaves[$result1->leavetype_tid]= 0;
  }
 }
  $balance=db_query("SELECT al.leavetype_tid from {leavetype} lt,{users_roles} ur,{assignleave} al WHERE lt.leavetypeid=al.leavetype_tid AND al.designation_rid=ur.rid AND ur.uid=%d",$account->uid);
  $bal=array();
 
  while($data2 = db_fetch_object($balance)){
   
    if($tot[$data2->leavetype_tid] - $leaves_taken[$data2->leavetype_tid]<0)
    {
      $bal[$data2->leavetype_tid]=0;
    }
    else
      $bal[$data2->leavetype_tid]= $tot[$data2->leavetype_tid] - $leaves_taken[$data2->leavetype_tid];
  }

  $form['leaves_taken'][$account->uid][0] = array('#value' =>  theme('item_list', $leaves_taken));
  $form['paidleaves'][$account->uid][0] = array('#value' =>  theme('item_list', $paidleaves));
  $form['nonpaidleaves'][$account->uid][0] = array('#value' =>  theme('item_list', $nonpaidleaves));
  $form['balance'][$account->uid][0] = array('#value' =>  theme('item_list', $bal));
}
  $form['accounts'] = array(
  '#type' => 'checkboxes',
  '#options' => $accounts
  );
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  return $form;
}

function theme_leavemgt_admin_account($form) {
  // Overview table:
    $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Username'), 'field' => 'u.name'),
    t('Roles'),
    array('data' => t('Leave Type')),
    array('data' => t('Leaves Allowed')),
    array('data' => t('Extra Assigned Leave')),
    array('data' => t('Total Leaves')),
    array('data' => t('Leaves Taken')),
    array('data' => t('Paid Leaves')),
    array('data' => t('Non paid Leaves')),
    array('data' => t('Balance')),
   );

  $output = drupal_render($form['options']);
  if (isset($form['name']) && is_array($form['name'])) {
    foreach (element_children($form['name']) as $key) {
      $rows[] = array(
        drupal_render($form['accounts'][$key]),
        drupal_render($form['name'][$key]),
        drupal_render($form['roles'][$key]),
        drupal_render($form['leavetype'][$key]),
        drupal_render($form['assleave'][$key]),
	drupal_render($form['grantextra'][$key]),
        drupal_render($form['totalleave'][$key]),
	drupal_render($form['leaves_taken'][$key]),
        drupal_render($form['paidleaves'][$key]),
        drupal_render($form['nonpaidleaves'][$key]),
	drupal_render($form['balance'][$key]),
      );
    }
  }
  else {
    $rows[] = array(array('data' => t('No users available.'), 'colspan' => '7'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  $output .= drupal_render($form);
  return $output;
}

 function myleaveprac() {
  global $user;
  $account= $user->uid;
  $result= db_query("SELECT la.nid,n.nid,n.uid,la.leavetype_tid,la.frmdate,la.todate,la.status,lt.leavetype,la.changedby FROM {leaveapp} la,{node} n, {leavetype} lt WHERE n.nid= la.nid AND la.leavetype_tid=lt.leavetypeid AND n.uid=$account AND la.created=(SELECT max(la.created) from leaveapp as la where n.nid=la.nid group by la.nid)");
  $myleaves=array();
  while($data = db_fetch_object($result)){
   
    $date1 = format_date($data->frmdate, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
    $date2 = format_date($data->todate, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
    $diff= (dateDiff("/",$date2,$date1)+1);
    $role=db_fetch_object(db_query("SELECT r.name from {role} r,{users_roles} ur where ur.uid=%d AND ur.rid=r.rid",$data->changedby));
    if($data->changedby==0){
      $myleaves['decision'] = 'Auto Decision';  
    }
    else if($data->changedby==$account){
      $myleaves['decision'] = 'Self';  
    }
    else
    $myleaves['decision'] = $role->name;
    $myleaves['frmdate'] = $date1;
    $myleaves['todate'] = $date2;
    $myleaves['total'] = $diff;
    $myleaves['leavetype'] = $data->leavetype;
    $myleaves['nid']= $data->nid;
    $leavestatus = db_fetch_object(db_query("SELECT leavestatus from {leavestatus} where leavestatusid=%d",$data->status));
    $myleaves['status'] = $leavestatus->leavestatus;
    $form['myleave']['leave'][] = array('#value' =>  $myleaves);
  }

  return $form;
}
 function theme_myleaveprac($form){
 
  $header = array(
    array('data' => t('From Date')),
    array('data' => t('To Date')),
    array('data' => t('No of Days')), 
    array('data' => t('leave Type')),
    array('data' => t('Decision Taken By')),
    array('data' => t('Status')),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $value = $form['myleave']['leave'];
  $cancel_node=array();
  if(isset($value[0]['#value']['nid'])){
  foreach ($value as $key => $value ) {
    if(is_numeric($key)) {
      if($value['#value']['status']=='cancel'){
        $cancel_node[$value['#value']['nid']]=$value['#value']['nid'];
      }
    }
  }
  }
  $value = $form['myleave']['leave'];
 
  if(isset($value[0]['#value']['nid'])){
    foreach ($value as $key => $value ) {
      if(is_numeric($key)) {
        $nid = $value['#value']['nid'];
        $row = array();
        $row[] = array('data'=>$value['#value']['frmdate']);
        $row[] = array('data'=>$value['#value']['todate']);
        $row[] = array('data'=>$value['#value']['total']);
        $row[] = array('data'=>$value['#value']['leavetype']);
        $row[] = array('data'=>$value['#value']['decision']);
        $row[] = array('data'=>$value['#value']['status']);
        
        if($value['#value']['nid'] == $cancel_node[$value['#value']['nid']]){
          $row[]=array('data'=>t('edit'));
          $row[]=array('data'=>t('cancel'));
        }
        else {
          $row[]=array('data'=>l('Edit','node/'.$nid.'/edit',array('query' => drupal_get_destination())));
          $row[]=array('data'=>l('Cancel','leavemgt/myleaves/'.$nid.'/cancel'));
        }
        $rows[] = $row;
    }
  }  
  }  
  else {
   $rows[]=array(array('data'=> t('Sorry there is no data.'),'colspan' => '8'));
  }
 
  return theme('table',$header,$rows);
}
function myleave_cancel(){
  //print_r($nid);
  $form = array();
  drupal_set_message($message='Do you really want to cancel your leave ?.',$type = 'status', $repeat = FALSE);
  $button = array();
  $form['button']['yes'] = array(
    '#type' => 'submit',
    '#value' => t('YES'),
    '#submit' => array('leavecancel_yes')
  );
  $form['button']['no'] = array(
    '#type' => 'submit',
    '#value' => t('NO'),
    '#submit' => array('leavecancel_no')
  );
  return $form;
}
function leavecancel_no($form,&$form_state){
  $form_state['redirect'] = 'leavemgt/myleaves';
  return;
}
function leavecancel_yes(){
  
  $nid= arg(2);
  $page=node_load($nid);
  $today_date=mktime(0,0,0,date("m"),date("d"),date("Y"));
  $past_date = $page->frmdate;
  $date1 = format_date($past_date, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
  
  
  if($today_date < $past_date){
   db_query('UPDATE {leaveapp} SET status = %d WHERE nid = %d', 4, $nid);
  }
  else{
    drupal_set_message($message='You can not cancel the leave.',$type='warning', $repeat = FALSE);
  }
  return;
}

function myleavesummary(){
  global $user;
  $account = $user->uid;
  $result= db_query("SELECT  max(al.maxallowleave) as max,al.leavetype_tid,lt.leavetype FROM {assignleave} al,{users_roles} ur,{leavetype} lt WHERE al.designation_rid=ur.rid  AND al.leavetype_tid = lt.leavetypeid AND ur.uid = %d group by al.leavetype_tid",$account);
  $myleavesummary=array();
  $leavetype=array();
  while($data= db_fetch_object($result)){
     $myleavesummary['leavetype'] = $data->leavetype;
     $myleavesummary['maxallowleave'] = $data->max;
    
  $res = db_query("SELECT sum(leavegranted) as total FROM {xtraleave} xl WHERE xl.leavetypeid=$data->leavetype_tid AND xl.uid=%d",$account);
  while($data1 = db_fetch_object($res)){
    if(!$data1->total){
       $data1->total =0;
     }
     $myleavesummary['grantextra'] = $data1->total;
     $myleavesummary['totalassign'] = $myleavesummary['maxallowleave']+$myleavesummary['grantextra'] ;
     
  }
  
  $takenleave = db_fetch_object(db_query('SELECT sum(la.totaldays) as total FROM {leaveapp} la,{node} n WHERE la.leavetype_tid=%d AND la.nid=n.nid AND n.uid= %d AND la.status=%d',$data->leavetype_tid,$account,1));
  $takenleave=$takenleave->total;
  if(!$takenleave){
    $takenleave=0;
  }
 // print_r($myleavesummary['totalassign']);
  $myleavesummary['leavetaken'] = $takenleave;
  if($takenleave>$myleavesummary['totalassign']){
    $nonpaid=$takenleave-$myleavesummary['totalassign'];
    $paidleave=$myleavesummary['totalassign'];
  }
  else{
    $paidleave=$takenleave;
    $nonpaid=0;
  }
  if($myleavesummary['totalassign']-$takenleave<0){
    $balance=0;
  }
  else{
    $balance=$myleavesummary['totalassign']-$takenleave;
  }
  $myleavesummary['paidleave'] = $paidleave;
  $myleavesummary['nonpaid'] = $nonpaid;
  $myleavesummary['balance'] = $balance;
  $form['myleavesummary']['leave'][] = array('#value' =>  $myleavesummary);
  }
   
 
  return $form;
}

function theme_myleavesummary($form){
  $header = array(
    array('data' => t('Leave Type')),
    array('data' => t('Leaves Allowed')),
    array('data' => t('Extra Assigned Leave')),
    array('data' => t('Total Assigned Leave')),
    array('data' => t('Leaves Taken')),
    array('data' => t('Paid Leaves')), 
    array('data' => t('Non Paid Leaves')),
    array('data' => t('Balance'))
    
  );
  
  $value = $form['myleavesummary']['leave'];
   if(!$value)
    $rows[]=array(array('data'=> t('Sorry there is no data.'),'colspan' => '8'));
  else {
  
  foreach ($value as $key => $value ) {
    if(is_numeric($key)) {
      $rows[] = array(
$value['#value']['leavetype'],$value['#value']['maxallowleave'],$value['#value']['grantextra'],$value['#value']['totalassign'],$value['#value']['leavetaken'],$value['#value']['paidleave'],$value['#value']['nonpaid'],$value['#value']['balance']
      );
    }
  }
  }
  return theme('table',$header,$rows);
}

function pendingleaves() {
  global $user;
  $account= $user->uid;
  $today_date=mktime(0,0,0,date("m"),date("d"),date("Y"));
  $authority = variable_get('authority',null);
 
  $role=db_fetch_object(db_query("SELECT r.name,r.rid from {role} r,{users_roles} ur WHERE ur.uid=%d AND ur.rid=r.rid",$account));
  
  if($role->rid== $authority){
    $result= db_query('SELECT lt.noticeperiod,la.nid,n.nid,n.uid,n.changed,la.leavetype_tid,la.frmdate,la.todate,u.name,la.status,lt.leavetype FROM {leaveapp} la, {node} n, {leavetype} lt,{users} u WHERE n.nid= la.nid AND la.leavetype_tid= lt.leavetypeid AND n.uid=u.uid AND la.frmdate > %d ORDER BY n.changed DESC',$today_date);  
  }
  else
    $result= db_query('SELECT lt.noticeperiod,la.nid,n.nid,n.uid,n.changed,la.leavetype_tid,la.frmdate,la.todate,u.name,la.status,lt.leavetype FROM {leaveapp} la, {node} n, {leavetype} lt,{users} u WHERE n.nid= la.nid AND la.leavetype_tid= lt.leavetypeid AND n.uid=u.uid AND la.reportto=%d AND la.frmdate > %d ORDER BY n.changed DESC',$account,$today_date);
  
 
  $res1= db_query('SELECT al.maxallowleave,al.designation_rid  FROM {assignleave} al');
  while($rs=db_fetch_object($res1)) {
    
    $tot[$rs->designation_rid]+=$rs->maxallowleave;
  }
  $i = 0;
  $rs=db_query("SELECT leavestatusid,leavestatus FROM {leavestatus} WHERE leavestatus.leavestatusid!=4");
  while($data = db_fetch_object($rs)){
    $status[$data->leavestatusid] = $data->leavestatus;
  }

  while($data = db_fetch_object($result)){
  $date1 = format_date($data->frmdate, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
  $date2 = format_date($data->todate, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
  $diff= (dateDiff("/",$date2,$date1)+1);
  $basic_status=leavemgt_basic_criteria($data->uid,$data->leavetype_tid,$date1,$diff);
  if($basic_status==1) {
       $image = theme('image', 'sites/all/modules/leavemgt/true.jpg');
  }
  else {
      $image = theme('image', 'sites/all/modules/leavemgt/false.jpg');
  }
   
    
    $nid = $data->nid;
    $form['id_'.$i]['username_'.$i] =array(
      '#type' => 'item', 
      '#value' => $data->name,  
    );
    $form['id_'.$i]['frmdate_'.$i] =array(
      '#type' => 'item', 
      '#value' => $date1,  
    );
    $form['id_'.$i]['todate_'.$i] =array(
      '#type' => 'item', 
      '#value' => $date2,  
    );
    $form['id_'.$i]['total_'.$i] =array(
      '#type' => 'item', 
      '#value' => $diff,  
    );
    $form['id_'.$i]['leavetype_'.$i] =array(
      '#type' => 'item', 
      '#value' => $data->leavetype,  
    );
    $form['id_'.$i]['status_'.$i] =array(
      '#type' => 'radios', 
      '#options' => $status,
      '#default_value' => $data->status,
    );
    $form['id_'.$i]['criteria_'.$i] =array(
      '#type' => 'item', 
      '#value' => $image,
    );
    $form['id_'.$i]['detail_'.$i] =array(
      '#type' => 'item', 
      '#value' => l('View','node/'.$nid,array('query' => drupal_get_destination())), // .'<br/>'. l('Edit','node/'.$nid.'/edit',array('query' => drupal_get_destination())),
    );
    $form['id_'.$i]['nid_'.$i] =array(
      '#type' => 'hidden', 
      '#value' => $nid,
    );
    $i++;
  }
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['#submit'][]='pending_leaves_submit';
  return $form;
  
}
function theme_pendingleaves($form) {
  $header = array(
    array('data' => t('User Name')),
    array('data' => t('Leave Type')),
    array('data' => t('From Date')),
    array('data' => t('To Date')),
    array('data' => t('No Of Days')),
    array('data' => t('Status'),'class'=>'radios'),
    array('data' => t('Satisfying basic crieteria')),
    array('data' => t('Operations')),
 
  );
  $i= 0;
  if(isset($form['id_'.$i]['username_'.$i])){
  foreach ($form['id_'.$i] as $key => $value) {
    $row = array();
    $row[] = drupal_render($form['id_'.$i]['username_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['leavetype_'.$i]);    
    $row[] = drupal_render($form['id_'.$i]['frmdate_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['todate_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['total_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['status_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['criteria_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['detail_'.$i]);
    $rows[] = $row;
    $i++;
  }
  }
  else {
    $rows[]=array(array('data'=> t('Sorry there is no any pending leave application.'),'colspan' => '8'));
  }
  $output .= theme('table',$header,$rows);
  $output .= drupal_render($form);
  return $output;
}

function pending_leaves_submit(&$form, &$form_state, $form_id = NULL) {
  $values = $form_state['values'];
  $id = 0;
  $i= 0;
  foreach ($values as $key => $value) {
    if($key == 'status_'.$id ) {
      $nid = $values['nid_'.$id];
      $status=db_fetch_object(db_query("SELECT status from {leaveapp}  where nid=%d",$nid));
      if($status->status!=$value) {
        $leavetype=$form['id_'.$i]['leavetype_'.$i];
        $from_date=$form['id_'.$i]['frmdate_'.$i];
        $to_date=$form['id_'.$i]['todate_'.$i];
        leavemgt_admin_mail($nid,$value,$leavetype['#value'],$from_date['#value'],$to_date['#value']);
      }
      db_query('UPDATE {leaveapp} SET status = %d WHERE nid = %d', $value, $nid);
    }
    if(($i%2) == 0) {
      $id++;
    }
    $i++;
  }
}
function leavemgt_admin_mail($nid,$statusid,$leavetypeid,$from_date,$to_date) {
  
  $typeid=db_fetch_object(db_query("SELECT leavetypeid from {leavetype} WHERE leavetype=%d",$leavetypeid));
  $stat=db_fetch_object(db_query("SELECT leavestatus from {leavestatus} WHERE leavestatusid=%d",$statusid));
  $uid=db_fetch_object(db_query("SELECT uid from {node} WHERE nid=%d",$nid));
  $diff= (dateDiff("/",$to_date,$from_date)+1);
  $pasttotal = db_fetch_object(db_query('SELECT sum(la.totaldays) as total FROM {leaveapp} la,{node} n WHERE la.leavetype_tid=%d AND la.nid=n.nid AND n.uid= %d AND la.status=%d', $typeid->leavetypeid,$uid->uid,1));
  if(!$pasttotal){
    $pasttotal->total=0;
  }
 
  if($statusid==1){
    $pasttotal->total+=$diff;
  }
 
  $max_allow = db_fetch_object(db_query('SELECT al.maxallowleave FROM {assignleave} al,{users_roles} ur,{users} u,{node} n WHERE al.leavetype_tid=%d AND  n.nid=%d AND n.uid=u.uid AND u.uid=ur.uid AND ur.rid=al.designation_rid',$typeid->leavetypeid,$nid));
  if($max_allow->maxallowleave - $pasttotal->total < 0){
    $bal=0;    
  }
  else {
    $bal= $max_allow->maxallowleave - $pasttotal->total;
  }
 
  $body = t('Your ');
  $body .= $leavetypeid;
  $body .= t(' from ');
  $body .= $from_date;
  $body .= t(' to ');
  $body .= $to_date;
  $body .= t(' is ');
  $body .= $stat->leavestatus;
  $body .= t("\n\n");
  $body .= t('Summary');
  $body .= t("\n\t\t\t");
  $body .= t('Total Taken Leave: ');
  $body .= $pasttotal->total;
  $body .= t("\n\t\t\t");
  $body .= t('Maximum Allowed Leave: ');
  $body .= $max_allow->maxallowleave;
  $body .= t("\n\t\t\t");
  $body .= t('Remaining Leave: ');
  $body .= $bal;
  
  if($bal==0){
    $body .= t("\n\n");
    $body .= t('Now if you take this type of leave it will be consider as Nonpaid.');
  }
  
  $title='Reply';
  $language = $language ? $language : language_default();
  $authority = variable_get('authority',null);
  $to=db_fetch_object(db_query("SELECT u.mail from {users} u,{node} n WHERE n.nid=%d AND n.uid=u.uid",$nid));
  $admin =db_fetch_object(db_query("SELECT u.mail from {users} u,{role} r,{users_roles} ur WHERE r.rid = $authority AND ur.rid=r.rid and ur.uid=u.uid"));
  $message=drupal_mail('leavemgt','email_recipient1',$to->mail,$language,array('subject' => $title,'body'=> $body),$admin->mail);
}


function pastleaves() {
  global $user;
  $account= $user->uid;
  $today_date=mktime(0,0,0,date("m"),date("d"),date("Y"));
  $authority = variable_get('authority',null); 
 
  $role=db_fetch_object(db_query("SELECT r.name,r.rid from {role} r,{users_roles} ur WHERE ur.uid=%d AND ur.rid=r.rid",$account));
  if($role->rid== $authority){
    $result= db_query('SELECT lt.noticeperiod,la.nid,n.nid,n.uid,n.changed,la.leavetype_tid,la.frmdate,la.todate,u.name,la.status,lt.leavetype FROM {leaveapp} la, {node} n, {leavetype} lt,{users} u WHERE n.nid= la.nid AND la.leavetype_tid= lt.leavetypeid AND n.uid=u.uid AND la.frmdate <= %d ORDER BY n.changed DESC',$today_date);  
  }
  else
    $result= db_query('SELECT lt.noticeperiod,la.nid,n.nid,n.uid,n.changed,la.leavetype_tid,la.frmdate,la.todate,u.name,la.status,lt.leavetype FROM {leaveapp} la, {node} n, {leavetype} lt,{users} u WHERE n.nid= la.nid AND la.leavetype_tid= lt.leavetypeid AND n.uid=u.uid AND la.reportto=%d AND la.frmdate <= %d ORDER BY n.changed DESC',$account,$today_date);
  
 
  $res1= db_query('SELECT al.maxallowleave,al.designation_rid  FROM {assignleave} al');
  while($rs=db_fetch_object($res1)) {
    
    $tot[$rs->designation_rid]+=$rs->maxallowleave;
  }
  $i = 0;
  $rs=db_query("SELECT leavestatusid,leavestatus FROM {leavestatus} WHERE leavestatus.leavestatusid!=4");
  while($data = db_fetch_object($rs)){
    $status[$data->leavestatusid] = $data->leavestatus;
  }

  while($data = db_fetch_object($result)){
   
    
  
   
    $date1 = format_date($data->frmdate, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
    $date2 = format_date($data->todate, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
    $diff= (dateDiff("/",$date2,$date1)+1);

    $basic_status=leavemgt_basic_criteria($data->uid,$data->leavetype_tid,$date1,$diff);
   
    if($basic_status==1) {
       $image = theme('image', 'sites/all/modules/leavemgt/true.jpg');
    }
    else {
       $image = theme('image', 'sites/all/modules/leavemgt/false.jpg');
    }
   
    
    $nid = $data->nid;
    $form['id_'.$i]['username_'.$i] =array(
      '#type' => 'item', 
      '#value' => $data->name,  
    );
    $form['id_'.$i]['frmdate_'.$i] =array(
      '#type' => 'item', 
      '#value' => $date1,  
    );
    $form['id_'.$i]['todate_'.$i] =array(
      '#type' => 'item', 
      '#value' => $date2,  
    );
    $form['id_'.$i]['total_'.$i] =array(
      '#type' => 'item', 
      '#value' => $diff,  
    );
    $form['id_'.$i]['leavetype_'.$i] =array(
      '#type' => 'item', 
      '#value' => $data->leavetype,  
    );
    $form['id_'.$i]['status_'.$i] =array(
      '#type' => 'radios', 
      '#options' => $status,
      '#default_value' => $data->status,
    );
    $form['id_'.$i]['criteria_'.$i] =array(
      '#type' => 'item', 
      '#value' => $image,
    );
    $form['id_'.$i]['detail_'.$i] =array(
      '#type' => 'item', 
      '#value' => l('View','node/'.$nid,array('query' => drupal_get_destination())), //.'<br/>'. l('Edit','node/'.$nid.'/edit',array('query' => drupal_get_destination())),
    );
    $form['id_'.$i]['nid_'.$i] =array(
      '#type' => 'hidden', 
      '#value' => $nid,
    );
    $i++;
  }
 
  return $form;
  
}

function theme_pastleaves($form) {
 
 
  $header = array(
    array('data' => t('User Name')),
    array('data' => t('Leave Type')),
    array('data' => t('From Date')),
    array('data' => t('To Date')),
    array('data' => t('No Of Days')),
    array('data' => t('Status'),'class'=>'radios'),
    array('data' => t('Satisfying basic crieteria')),
    array('data' => t('Operations')),
 
  );
  $i= 0;
  if(isset($form['id_'.$i]['username_'.$i])){
  foreach ($form['id_'.$i] as $key => $value) {
    $row = array();
    $row[] = drupal_render($form['id_'.$i]['username_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['leavetype_'.$i]);    
    $row[] = drupal_render($form['id_'.$i]['frmdate_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['todate_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['total_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['status_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['criteria_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['detail_'.$i]);
    $rows[] = $row;
    $i++;
  }
  }
  else {
    $rows[]=array(array('data'=> t('Sorry there is no any past leave application.'),'colspan' => '8'));
  }
  $output .= theme('table',$header,$rows);
  $output .= drupal_render($form);
  
  return $output;
}

function cancelleaves(){
  global $user;
  $account= $user->uid;
  $today_date=mktime(0,0,0,date("m"),date("d"),date("Y"));
   
  $role=db_fetch_object(db_query("SELECT r.name from {role} r,{users_roles} ur WHERE ur.uid=%d AND ur.rid=r.rid",$account));
  if($role->name=='Administrator'){
    $result= db_query('SELECT la.nid,n.nid,n.uid,la.leavetype_tid,la.frmdate,la.todate,u.name,lt.leavetype FROM {leaveapp} la, {node} n, {leavetype} lt,{users} u WHERE n.nid= la.nid AND la.leavetype_tid= lt.leavetypeid AND n.uid=u.uid AND la.frmdate >= %d AND la.status=%d ORDER BY n.changed DESC',$today_date,4);  
  }
  else
    $result= db_query('SELECT la.nid,n.nid,n.uid,la.leavetype_tid,la.frmdate,la.todate,u.name,lt.leavetype FROM {leaveapp} la, {node} n, {leavetype} lt,{users} u WHERE n.nid= la.nid AND la.leavetype_tid= lt.leavetypeid AND n.uid=u.uid AND la.reportto=%d AND la.frmdate >= %d AND la.status=%d ORDER BY n.changed DESC',$account,$today_date,4);
  $i=0;
  while($data = db_fetch_object($result)){
   
    $date1 = format_date($data->frmdate, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
    $date2 = format_date($data->todate, $type = 'custom', $format = 'd/m/Y', $timezone = NULL, $langcode = NULL);
    $diff= (dateDiff("/",$date2,$date1)+1);
    $nid = $data->nid;
    $form['id_'.$i]['username_'.$i] =array(
      '#type' => 'item', 
      '#value' => $data->name,  
    );
    $form['id_'.$i]['frmdate_'.$i] =array(
      '#type' => 'item', 
      '#value' => $date1,  
    );
    $form['id_'.$i]['todate_'.$i] =array(
      '#type' => 'item', 
      '#value' => $date2,  
    );
    $form['id_'.$i]['total_'.$i] =array(
      '#type' => 'item', 
      '#value' => $diff,  
    );
    $form['id_'.$i]['leavetype_'.$i] =array(
      '#type' => 'item', 
      '#value' => $data->leavetype,  
    );
   
    $form['id_'.$i]['nid_'.$i] =array(
      '#type' => 'hidden', 
      '#value' => $nid,
    );
    $i++;
  }
 
  return $form;
  
}

function theme_cancelleaves($form) {
 
 
  $header = array(
    array('data' => t('User Name')),
    array('data' => t('Leave Type')),
    array('data' => t('From Date')),
    array('data' => t('To Date')),
    array('data' => t('No Of Days')),
    
  );
  $i= 0;
 
  if(isset($form['id_'.$i]['username_'.$i])){
  foreach ($form['id_'.$i] as $key => $value) {
    $row = array();
    $row[] = drupal_render($form['id_'.$i]['username_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['leavetype_'.$i]);    
    $row[] = drupal_render($form['id_'.$i]['frmdate_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['todate_'.$i]);
    $row[] = drupal_render($form['id_'.$i]['total_'.$i]);
    
    $rows[] = $row;
    $i++;
  }
  }
  else {
    $rows[]=array(array('data'=> t('Sorry there is no any cancel leaves.'),'colspan' => '5'));
  }
  $output .= theme('table',$header,$rows);
  $output .= drupal_render($form);
  
  return $output;
}


